# Provisioning Multiple ECS Services Using Terraform

This repository contains the Terraform implementaton for creating an ECS cluster with multiple ECS services.
You can find the step by step instructions from this [article](https://medium.com/towards-aws/provisioning-multiple-ecs-services-using-terraform-d4448354c803).

## Architecture


![ECS Service Communication Using an Internal Load Balancer](ecs.png?raw=true)

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 3.0 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ecr"></a> [ecr](#module\_ecr) | ./modules/ecr | n/a |
| <a name="module_ecs"></a> [ecs](#module\_ecs) | ./modules/ecs | n/a |
| <a name="module_iam"></a> [iam](#module\_iam) | ./modules/iam | n/a |
| <a name="module_internal_alb"></a> [internal\_alb](#module\_internal\_alb) | ./modules/alb | n/a |
| <a name="module_internal_alb_security_group"></a> [internal\_alb\_security\_group](#module\_internal\_alb\_security\_group) | ./modules/security-group | n/a |
| <a name="module_public_alb"></a> [public\_alb](#module\_public\_alb) | ./modules/alb | n/a |
| <a name="module_public_alb_security_group"></a> [public\_alb\_security\_group](#module\_public\_alb\_security\_group) | ./modules/security-group | n/a |
| <a name="module_route53_private_zone"></a> [route53\_private\_zone](#module\_route53\_private\_zone) | ./modules/route53 | n/a |
| <a name="module_vpc"></a> [vpc](#module\_vpc) | ./modules/vpc | n/a |

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account"></a> [account](#input\_account) | AWS account number | `number` | n/a | yes |
| <a name="input_app_name"></a> [app\_name](#input\_app\_name) | Application name | `string` | n/a | yes |
| <a name="input_app_services"></a> [app\_services](#input\_app\_services) | service name list | `list(string)` | n/a | yes |
| <a name="input_availability_zones"></a> [availability\_zones](#input\_availability\_zones) | Availability zones that the services are running | `list(string)` | n/a | yes |
| <a name="input_cidr"></a> [cidr](#input\_cidr) | VPC CIDR | `string` | n/a | yes |
| <a name="input_env"></a> [env](#input\_env) | Environment | `string` | n/a | yes |
| <a name="input_internal_alb_config"></a> [internal\_alb\_config](#input\_internal\_alb\_config) | Internal ALB configuration | <pre>object({<br>    name      = string<br>    listeners = map(object({<br>      listener_port     = number<br>      listener_protocol = string<br>    }))<br>    ingress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>    }))<br>    egress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>    }))<br>  })</pre> | n/a | yes |
| <a name="input_internal_url_name"></a> [internal\_url\_name](#input\_internal\_url\_name) | Friendly url name for the internal load balancer DNS | `string` | n/a | yes |
| <a name="input_microservice_config"></a> [microservice\_config](#input\_microservice\_config) | Microservice configuration | <pre>map(object({<br>    name           = string<br>    is_public      = bool<br>    container_port = number<br>    host_port = number<br>    cpu            = number<br>    memory         = number<br>    desired_count  = number<br><br>    alb_target_group = object({<br>      port              = number<br>      protocol          = string<br>      path_pattern      = list(string)<br>      health_check_path = string<br>      priority          = number<br>    })<br><br>    auto_scaling = object({<br>      max_capacity = number<br>      min_capacity = number<br>      cpu          = object({<br>        target_value = number<br>      })<br>      memory = object({<br>        target_value = number<br>      })<br>    })<br>  }))</pre> | n/a | yes |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | Private subnets | `list(string)` | n/a | yes |
| <a name="input_public_alb_config"></a> [public\_alb\_config](#input\_public\_alb\_config) | Public ALB configuration | <pre>object({<br>    name      = string<br>    listeners = map(object({<br>      listener_port     = number<br>      listener_protocol = string<br>    }))<br>    ingress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>    }))<br>    egress_rules = list(object({<br>      from_port   = number<br>      to_port     = number<br>      protocol    = string<br>      cidr_blocks = list(string)<br>    }))<br>  })</pre> | n/a | yes |
| <a name="input_public_subnets"></a> [public\_subnets](#input\_public\_subnets) | Public subnets | `list(string)` | n/a | yes |
| <a name="input_region"></a> [region](#input\_region) | region | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->